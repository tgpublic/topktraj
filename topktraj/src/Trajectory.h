#ifndef TOPKTRAJ_TRAJECTORY_H
#define TOPKTRAJ_TRAJECTORY_H

#include <utility>
#include <vector>
#include "Graph.h"


extern GraphP graph;

using TrajectoryId = unsigned long;
using TrajectoryIds = std::vector<TrajectoryId>;
using Time = unsigned long;

struct TimeInterval {
    Time start;
    Time end;
};

struct TraPoint {
    NodeId nid;
    TimeInterval interval;
    Id trid;
};

class Trajectory {

public:

    Trajectory& operator=(const Trajectory &t) {
        if (this == &t) return *this;
        id = t.id;
        points = t.points;
        isPivot = t.isPivot;
        return *this;
    }

    void initPivot(NodeId nid, TimeInterval ti);

    [[nodiscard]] TimeInterval getTimeInterval() const {
        return {points.front().interval.start, points.back().interval.end};
    }

    [[nodiscard]] Time getStartTime() const {
        return points.front().interval.start;
    }

    [[nodiscard]] Time getEndTime() const {
        return points.back().interval.end;
    }

    TrajectoryId id;
    std::vector<TraPoint> points;
    bool isPivot = false;

    bool usetr = true;
};


using Trajectories = std::vector<Trajectory>;

std::ostream& operator<<(std::ostream& os, const Trajectory &t);

bool timeIntervalsIntersect(const TimeInterval &a, const TimeInterval &b);

unsigned long sizeOfTimeInterval(const TimeInterval &t);

unsigned long sizeOfCutTimeIntervals(const TimeInterval &a, const TimeInterval &b);

unsigned long sizeOfCutTimeIntervals(const TimeInterval &a, const TimeInterval &b, const TimeInterval &t);

std::string to_string(const TimeInterval &t);

Trajectory shrink(const Trajectory &trajectory);

Trajectory shrink_RG(const Trajectory &trajectory);

void validate(Trajectory &trajectory);

#endif //TOPKTRAJ_TRAJECTORY_H
