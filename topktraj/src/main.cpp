#include <iostream>
#include <chrono>
#include <cmath>
#include <unordered_set>
#include <numeric>
#include "DataLoader.h"
#include "Trajectory.h"
#include "TrajectoryIndex.h"
#include "BaselineIndex.h"
#include "SGLog.h"
#include "Params.h"

using namespace std;

GraphP graph = make_shared<Graph>();
GraphP reduced_graph = make_shared<Graph>();


void get_mean_std(vector<double> &values, double &mean, double &stdev) {
    double sum = std::accumulate(std::begin(values), std::end(values), 0.0);
    mean = sum / values.size();
    double accum = 0.0;
    std::for_each (std::begin(values), std::end(values), [&](const double d) {
        accum += (d - mean) * (d - mean);
    });
    stdev = sqrt(accum / (values.size()));
}


TopkResults run_baseline_topk(Trajectories trajectories, Trajectories queries, unsigned int k) {
    SGLog::log() << "running baseline" << endl;
    vector<TopkResult> topkResults;
    vector<double> result_sizes;
    vector<double> times;
    for (auto &Q : queries) {
        auto start = std::chrono::steady_clock::now();
        auto topkResult = kNNQueryNaiveWithBound(trajectories, Q, Q.getTimeInterval(), k);
        auto finish = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        times.push_back(elapsed.count());
        topkResults.push_back(topkResult);
        result_sizes.push_back(topkResult.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(times, mean, stdev);
    SGLog::log() << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(result_sizes, mean, stdev);
    SGLog::log() << "result size " << mean << " +- " << stdev << endl << endl;
    return topkResults;
}


TopkResults run_sea_baseline_approx(shared_ptr<AbstractIndex> index, Trajectories &trajectories, Trajectories &queries, unsigned int k) {
    SGLog::log() << "running sea baseline approx" << endl;
    vector<TopkResult> topkResults;
    vector<double> subset_sizes;
    vector<double> result_sizes;
    vector<double> times;
    for (auto &Q : queries) {
        auto shrunk_Q = shrink(Q);
        auto query_interval = shrunk_Q.getTimeInterval();
        auto start = std::chrono::steady_clock::now();
        Trajectories subset = index->query(shrunk_Q, query_interval, 0);
        subset_sizes.push_back(subset.size());
        auto topkResult = kNNQueryNaive(subset, Q, Q.getTimeInterval(), k, graph->distances);
        auto finish = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        times.push_back(elapsed.count());
        topkResults.push_back(topkResult);
        result_sizes.push_back(topkResult.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(times, mean, stdev);
    SGLog::log() << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(subset_sizes, mean, stdev);
    SGLog::log() << "subset size " << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(result_sizes, mean, stdev);
    SGLog::log() << "result size " << mean << " +- " << stdev << endl << endl;
    return topkResults;
}


TopkResults run_sea_baseline_approx_SHQT(shared_ptr<AbstractIndex> index, Trajectories &trajectories, Trajectories &queries, unsigned int k) {
    SGLog::log() << "running sea baseline approx" << endl;
    vector<TopkResult> topkResults;
    vector<double> times;
    vector<double> subset_sizes;
    vector<double> result_sizes;
    for (auto &Q : queries) {
        auto start = std::chrono::steady_clock::now();
        auto shrunk_Q = shrink(Q);
        auto query_interval = shrunk_Q.getTimeInterval();
        Trajectories subset = index->query(shrunk_Q, query_interval, 0);
        subset_sizes.push_back(subset.size());
        Trajectories result;
        for (auto &t : subset) {
            result.push_back(trajectories.at(t.id));
        }
        auto topkResult = kNNQueryNaive(result, Q, Q.getTimeInterval(), k, graph->distances);
        auto finish = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        times.push_back(elapsed.count());
        topkResults.push_back(topkResult);
        result_sizes.push_back(topkResult.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(times, mean, stdev);
    SGLog::log() << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(subset_sizes, mean, stdev);
    SGLog::log() << "subset size " << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(result_sizes, mean, stdev);
    SGLog::log() << "result size " << mean << " +- " << stdev << endl << endl;
    return topkResults;
}


void evaluate_results(TopkResults const &topkResults1, TopkResults const &topkResults2) {
    vector<double> issrs;
    for (int i = 0; i < topkResults1.size(); ++i) {
        auto topk_exact = topkResults1.at(i);

        auto topk = topkResults2.at(i);
        auto d = topk_exact.countTopkSet(topk);
        issrs.push_back(d.ssr_inv);
    }
    double mean = 0, stdev = 0;
    get_mean_std(issrs, mean, stdev);
    SGLog::log() << "ssr: \t\t" << mean << " +- " << stdev << endl;
}


TopkResults run_topk(const shared_ptr<AbstractIndex>& index, Trajectories trajectories, Trajectories queries, unsigned int k, double radius) {
    SGLog::log() << "running " << index->getName() << endl;
    vector<TopkResult> topkResults;
    vector<double> times;
    vector<double> subset_sizes;
    vector<double> result_sizes;
    for (auto &Q : queries) {
        auto query_interval = Q.getTimeInterval();
        index->reset();
        auto start = std::chrono::steady_clock::now();
        Trajectories subset = index->query(Q, query_interval, radius);
        subset_sizes.push_back(subset.size());
        auto topkResult = kNNQueryNaiveWithBound(subset, Q, Q.getTimeInterval(), k);
        auto finish = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        times.push_back(elapsed.count());
        topkResults.push_back(topkResult);
        result_sizes.push_back(topkResult.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(times, mean, stdev);
    SGLog::log() << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(subset_sizes, mean, stdev);
    SGLog::log() << "subset size " << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(result_sizes, mean, stdev);
    SGLog::log() << "result size " << mean << " +- " << stdev << endl << endl;
    return topkResults;
}

TopkResults run_topk_sea(const shared_ptr<AbstractIndex>& index, Trajectories trajectories, Trajectories queries, unsigned int k, double radius) {
    SGLog::log() << "running " << index->getName() << endl;
    vector<TopkResult> topkResults;
    vector<double> times;
    vector<double> subset_sizes;
    vector<double> result_sizes;
    for (auto &Q : queries) {
        auto query_interval = Q.getTimeInterval();
        index->reset();
        auto start = std::chrono::steady_clock::now();
        Trajectories subset = index->query(Q, query_interval, radius);
        subset_sizes.push_back(subset.size());
        auto topkResult = kNNQueryNaive(subset, Q, Q.getTimeInterval(), k, graph->distances);
        auto finish = std::chrono::steady_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        times.push_back(elapsed.count());
        topkResults.push_back(topkResult);
        result_sizes.push_back(topkResult.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(times, mean, stdev);
    SGLog::log() << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(subset_sizes, mean, stdev);
    SGLog::log() << "subset size " << mean << " +- " << stdev << endl;
    mean = 0, stdev = 0;
    get_mean_std(result_sizes, mean, stdev);
    SGLog::log() << "result size " << mean << " +- " << stdev << endl << endl;
    return topkResults;
}



void run_index_topk(const shared_ptr<AbstractIndex>& index, Trajectories trajectories_, const Trajectories& queries_, const vector<unsigned int>& ks, double radius, unsigned int index_num) {
    for (auto k : ks) {
        auto trajectories = trajectories_;
        auto queries = queries_;

        SGLog::log() << "k=" << k << endl;
        TopkResults topkResults_sea_bl, topkResults_sea_approx, topkResults_sea_approx2;

        auto topkResults_exact = run_baseline_topk(trajectories, queries, k);

        trajectories = trajectories_;
        queries = queries_;

        TopkResults topkResults_index;
        if (index_num == 0 || index_num == 1)
            topkResults_index = run_topk(index, trajectories, queries, k, radius);
        else if (index_num == 2)
            topkResults_index = run_topk_sea(index, trajectories, queries, k, radius);
        else if (index_num == 3)
            topkResults_index = run_sea_baseline_approx(index, trajectories, queries, k);
        else if (index_num == 4)
            topkResults_index = run_sea_baseline_approx_SHQT(index, trajectories, queries, k);

        SGLog::log() << "results for " + index->getName() + ": " << endl;
        evaluate_results(topkResults_exact, topkResults_index);

        SGLog::log() << endl;
    }
}


TimeInterval get_global_timeinterval(Trajectories &trajectories) {
    TimeInterval gt{UINT64_MAX, 0};
    for (auto &t : trajectories) {
        if (t.getStartTime() < gt.start) gt.start = t.getStartTime();
        if (t.getEndTime() > gt.end) gt.end = t.getEndTime();
    }
    SGLog::log() << to_string(gt) << endl << endl;
    return gt;
}


void run_experiment(const Params &params) {
    Trajectories trajectories, queries;
    auto graphfile = params.dataset_path + "_graph.txt";
    auto trajectoryfile = params.dataset_path + "_trajectories.txt";

    load_data(graphfile, trajectoryfile, trajectories, queries, params.num_queries);
    loadReducedGraph(trajectories);

    SGLog::log() << "# nodes: " << graph->getNumNodes() << endl;
    SGLog::log() << "# edges: " << graph->getNumEdges() << endl;
    SGLog::log() << "# trajectories: " << trajectories.size() << endl;
    SGLog::log() << "# queries: " << queries.size() << endl;

    shared_ptr<AbstractIndex> index = getIndex(params, trajectories, get_global_timeinterval(trajectories));

    run_index_topk(index, trajectories, queries, params.k, params.radius, params.index);
}


void print_length(Trajectories &trajectories) {
    vector<double> sizes;
    for (auto &t : trajectories) {
        sizes.push_back(t.points.size());
    }
    double mean = 0, stdev = 0;
    get_mean_std(sizes, mean, stdev);
    SGLog::log() << "trajectories size: " << mean << " +- "  << stdev << endl;
}


void get_stats(const Params &params) {
    Trajectories trajectories, queries;
    auto graphfile = params.dataset_path + "_graph.txt";
    auto trajectoryfile = params.dataset_path + "_trajectories.txt";
    load_data(graphfile, trajectoryfile, trajectories, queries, params.num_queries);
    loadReducedGraph(trajectories);
    vector<unsigned long> deg;
    unsigned long max_deg = 0;
    vector<vector<bool>> found(graph->getNumNodes(), vector<bool>(graph->getNumNodes(), false));
    unsigned long total = 0;
    unsigned long total2 = 0;
    for (auto &n : graph->nodes) {
        if (n->adjlist->getNumEdges() > max_deg) max_deg = n->adjlist->getNumEdges();
        deg.push_back(n->adjlist->getNumEdges());
        for (auto e : *n->adjlist) {
            if (!found.at(e->u->id).at(e->v->id)) {
                found.at(e->u->id).at(e->v->id) = true;
                total2++;
            }
        }
        total += n->adjlist->getNumEdges();
    }
    SGLog::log() << "# nodes: " << graph->getNumNodes() << endl;
    SGLog::log() << "# edges: " << graph->getNumEdges() << endl;
    SGLog::log() << "max. deg: " << max_deg << endl;
    SGLog::log() << "num edges: " << total << " " << total2 << endl;
    SGLog::log() << "# trajectories: " << trajectories.size() << endl;
    SGLog::log() << "# queries: " << queries.size() << endl;
    SGLog::log() << "total trajectories: " << trajectories.size() + queries.size() << endl;
    print_length(trajectories);
    print_length(queries);
    trajectories.clear();
    queries.clear();
}



int main(int argc, char* argv[]) {

    vector<string> args;
    for (size_t i = 1; i < argc; ++i) args.emplace_back(argv[i]);

    Params params;
    if (!params.parseArgs(args)) {
        show_help();
        return 1;
    }
    srand(params.rnd_seed);

    switch (params.mode) {
        case 0: {
            run_experiment(params);
            break;
        }
        case 1: {
            get_stats(params);
            break;
        }
        default: {
            show_help();
            return 1;
        }
    }

    return 0;
}

