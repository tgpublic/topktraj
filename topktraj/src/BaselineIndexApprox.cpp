#include <chrono>
#include "BaselineIndexApprox.h"
#include "SGLog.h"

using namespace std;

void BaselineIndexApprox::init(Trajectories trajectories1) {
    trajectories = std::move(trajectories1);

    auto cmp = [](TPair a, TPair b) {
        return a.trid < b.trid || a.interval.start < b.interval.start || a.interval.end < b.interval.end;
    };
    vector<set<TPair, decltype(cmp)>> tpairs(reduced_graph->getNumNodes(), set<TPair, decltype(cmp)>(cmp));

    for (auto &t : trajectories) {
        for (auto &p : t.points) {

            TPair tpair{};
            tpair.interval = p.interval;
            tpair.trid = t.id;
            tpair.nid = reduced_graph->nid_map.at(p.nid);
            assert(t.id == p.trid);
            tpairs.at(tpair.nid).insert(tpair);

            auto node = graph->getNodeById(reduced_graph->nid_map.at(p.nid));
            for (auto e : *node->adjlist) {
                TPair tp{};
                tp.interval = p.interval;
                tp.trid = t.id;
                tp.nid = reduced_graph->nid_map.at(e->v->id);
                tpairs.at(tp.nid).insert(tp);
            }
        }
    }

    for (auto &tpair : tpairs) {

        TPairs tpairs_vec(tpair.begin(), tpair.end());
        sort(tpairs_vec.begin(), tpairs_vec.end(), [](TPair const &a, TPair const &b){return a.interval.start < b.interval.start;});
        stable_sort(tpairs_vec.begin(), tpairs_vec.end(), [](TPair const &a, TPair const &b){return a.trid < b.trid;});
        TPairs np;
        for (int i = 0; i < tpairs_vec.size(); ++i) {
            TPair p = tpairs_vec.at(i);
            while (i < tpairs_vec.size()-1 && tpairs_vec.at(i+1).nid == p.nid && tpairs_vec.at(i+1).trid == p.trid && tpairs_vec.at(i+1).interval.start == p.interval.end) {
                i++;
            }
            p.interval.end = tpairs_vec.at(i).interval.end;
            np.push_back(p);
        }

        intervaltrees.emplace_back();
        intervaltrees.back().construct(np);
    }
}

Trajectories BaselineIndexApprox::query(Trajectory &Q, TimeInterval &t, double) {
    set<TrajectoryId> trid_set;
    for (auto &p : Q.points) {
        auto trids = intervaltrees.at(reduced_graph->nid_map.at(p.nid)).query(p.interval);
        trid_set.insert(trids.begin(), trids.end());
    }

    Trajectories result;
    for (auto &id : trid_set)
        result.push_back(trajectories.at(id));

    return result;
}


BaselineIndexApprox init_baseline_approx_SHQ(Trajectories const &trajectories) {
    SGLog::log() << "init sea baseline approx" << endl;
    auto start = std::chrono::steady_clock::now();
    BaselineIndexApprox bla;
    bla.init(trajectories);
    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "initialized sea baseline SHQ approx index in " << elapsed.count() << " s" << endl;
    return bla;
}


BaselineIndexApprox init_baseline_approx_SHQT(const Trajectories& trajectories) {
    SGLog::log() << "shrinking trajectories" << endl;
    auto start = std::chrono::steady_clock::now();
    Trajectories shrunk_trajectories;
    for (auto &t : trajectories)
        shrunk_trajectories.push_back(shrink_RG(t));
    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "shrinking took " << elapsed.count() << " s" << endl;
    SGLog::log() << "init sea baseline approx" << endl;
    start = std::chrono::steady_clock::now();
    BaselineIndexApprox bla;
    bla.init(std::move(shrunk_trajectories));
    finish = std::chrono::steady_clock::now();
    elapsed = finish - start;
    SGLog::log() << "initialized sea baseline SHQT approx index in " << elapsed.count() << " s" << endl;
    return bla;
}