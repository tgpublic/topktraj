#include <queue>
#include <cassert>
#include <cmath>
#include "Graph.h"

using namespace std;

void Graph::calculateSpatialDistances() {

    using L = pair<double, NodeP>;

    for (NodeId id = 0; id < getNumNodes(); ++id) {

        distances.resize(getNumNodes(), vector<double>(getNumNodes(), std::numeric_limits<double>::max()));
        distances.at(id).at(id) = 0;

        set<L> q;

        L init{0, nodes.at(id)};

        q.insert(init);
        vector<bool> fin(getNumNodes(), false);

        while (!q.empty()) {
            auto cur = q.begin();

            auto node = cur->second;
            auto nid = node->id;
            auto dist = cur->first;
            q.erase(q.begin());

            if (fin.at(nid)) continue;
            if (distances.at(id).at(nid) > dist)
                distances.at(id).at(nid) = dist;
            fin.at(nid) = true;

            for (shared_ptr<Edge> e : *node->adjlist) {
                L next = {dist + e->costs, e->v};
                q.insert(next);
            }
        }
    }
}

NodeP Node::getRandomNeighbour() const {
    unsigned long r = rand() % adjlist->getNumEdges();
    EdgeP e = adjlist->firstOut;
    while (r > 0) {
        if (e == adjlist->lastOut) break;
        assert(e->next != nullptr);
        e = e->next;
        r--;
    }
    return e->v;
}

void Adjlist::addEdge(const EdgeP &e)  {
    if (firstOut == nullptr && lastOut == nullptr) {
        firstOut = lastOut = e;
    } else {
        e->next = nullptr;
        e->prev = lastOut;
        lastOut->next = e;
        lastOut = e;
    }
    e->v->incomingEdges.insert(e);
    numEdges++;
}

void Adjlist::addEdge(NodeP u, NodeP v, double costs) {
    EdgeP e = std::make_shared<Edge>();
    e->u = std::move(u);
    e->v = std::move(v);
    e->costs = costs;
    addEdge(e);
}

