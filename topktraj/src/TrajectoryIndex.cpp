#include <cmath>
#include <cassert>
#include "TrajectoryIndex.h"
#include "TopkResult.h"

using namespace std;

TopkResult kNNQueryNaive(const Trajectories &trajectories, const Trajectory &Q, TimeInterval t, unsigned long k, std::vector<std::vector<double>> &distances) {
    TopkResult topkResult(k);
    for (const auto& T : trajectories) {
        auto simQT = sim(Q, T, t, distances);
        assert(simQT <= 1.0);
        if (simQT == -1) continue;
        assert(simQT >= 0);
        topkResult.insert(T.id, simQT);
    }
    return topkResult;
}

TopkResult kNNQueryNaiveWithBound(const Trajectories &trajectories, const Trajectory &Q, TimeInterval t, unsigned long k) {
    TopkResult topkResult(k);
    auto &distances = graph->distances;
    for (const auto& T : trajectories) {
        double b = -1;
        if (topkResult.doApproximation())
            b = topkResult.getMinMaxTopk();
        auto simQT = sim(Q, T, t, b, distances);
        assert(simQT <= 1.0);
        if (simQT == -1) continue;
        assert(simQT >= 0);
        topkResult.insert(T.id, simQT);
    }
    return topkResult;
}

double sim(const Trajectory& Q, const Trajectory& T, const TimeInterval &t, double lowerBound, std::vector<std::vector<double>> &distances) {

    if (T.getStartTime() > t.end || T.getEndTime() < t.start) return 0;
    if (Q.getStartTime() > t.end || Q.getEndTime() < t.start) return 0;

    auto t_size = (double)sizeOfTimeInterval(t);
    double result = 0.0;

    int pt = 0, pq = 0;
    TimeInterval at{};
    if (T.points.back().interval.end < Q.points.back().interval.end) {
        at.end = T.points.back().interval.end;
    } else {
        at.end = t.end;
    }

    while (pt < T.points.size() && pq < Q.points.size()) {

        assert(T.points.at(pt).interval.start < T.points.at(pt).interval.end);
        assert(Q.points.at(pq).interval.start < Q.points.at(pq).interval.end);

        if (T.points.at(pt).interval.end < t.start) {
            ++pt;
            continue;
        }
        if (Q.points.at(pq).interval.end < t.start) {
            ++pq;
            continue;
        }
        
        auto s = sizeOfCutTimeIntervals(T.points.at(pt).interval, Q.points.at(pq).interval, t);
        assert(s>=0);

        NodeId vid = T.points.at(pt).nid;
        NodeId uid = Q.points.at(pq).nid;
        double distance = distances.at(uid).at(vid);
        if (distance != std::numeric_limits<double>::max()) {
            auto e = exp(-1.0 * (double) distance);
            assert(e >= 0);
            result += s * e;
        }

        if (T.points.at(pt).interval.end < Q.points.at(pq).interval.end) {
            at.start = T.points.at(pt).interval.end;
        } else {
            at.start = Q.points.at(pq).interval.end;
        }

        at.end = t.end;
        double approx = (result + (double)sizeOfCutTimeIntervals(at, t)) / t_size;

        if (approx < lowerBound) {
            return -1;
        }

        if (pq == Q.points.size()-1 || T.points.at(pt).interval.end < Q.points.at(pq).interval.end) {
            pt++;
        } else if (pt == T.points.size()-1 || T.points.at(pt).interval.end > Q.points.at(pq).interval.end) {
            pq++;
        } else {
            pt++;
            pq++;
        }
    }
    result /= t_size;
    return result;
}


double sim(const Trajectory& Q, const Trajectory& T, const TimeInterval &t, std::vector<std::vector<double>> &distances) {

    if (T.getStartTime() > t.end || T.getEndTime() < t.start) return 0;
    if (Q.getStartTime() > t.end || Q.getEndTime() < t.start) return 0;

    auto t_size = (double)sizeOfTimeInterval(t);
    double result = 0.0;

    int pt = 0, pq = 0;
    while (pt < T.points.size() && pq < Q.points.size()) {

        if (T.points.at(pt).interval.end < t.start) {
            ++pt;
            continue;
        }
        if (Q.points.at(pq).interval.end < t.start) {
            ++pq;
            continue;
        }

        auto s = sizeOfCutTimeIntervals(T.points.at(pt).interval, Q.points.at(pq).interval, t);
        assert(s >= 0);

        NodeId vid = T.points.at(pt).nid;
        NodeId uid = Q.points.at(pq).nid;
        double distance = distances.at(uid).at(vid);

        if (distance != std::numeric_limits<double>::max()) {
            auto e = exp(-1.0 * (double) distance);
            assert(e >= 0);
            result += s * e;
        }

        if (pq == Q.points.size()-1 || T.points.at(pt).interval.end < Q.points.at(pq).interval.end) {
            pt++;
        } else if (pt == T.points.size()-1 || T.points.at(pt).interval.end > Q.points.at(pq).interval.end) {
            pq++;
        } else {
            pt++;
            pq++;
        }
    }
    result /= t_size;
    assert(result >= 0);
    return result;
}


