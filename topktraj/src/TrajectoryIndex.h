#ifndef TOPKTRAJ_TRAJECTORYINDEX_H
#define TOPKTRAJ_TRAJECTORYINDEX_H

#include <utility>
#include <algorithm>
#include "Trajectory.h"
#include "TopkResult.h"

TopkResult kNNQueryNaive(const Trajectories &trajectories, const Trajectory &Q, TimeInterval t, unsigned long k, std::vector<std::vector<double>> &distances);

TopkResult kNNQueryNaiveWithBound(const Trajectories &trajectories, const Trajectory &Q, TimeInterval t, unsigned long k);

double sim(const Trajectory& Q, const Trajectory& T, const TimeInterval &t, double lowerBound, std::vector<std::vector<double>> &distances);

double sim(const Trajectory& Q1, const Trajectory& T1, const TimeInterval &t, std::vector<std::vector<double>> &distances);

#endif //TOPKTRAJ_TRAJECTORYINDEX_H
