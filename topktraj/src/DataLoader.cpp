#include "DataLoader.h"
#include "SGLog.h"
#include <cstdlib>
#include <iostream>
#include <unordered_map>
#include <iomanip>
#include <cmath>
#include <algorithm>

using namespace std;

void getFileStream(const string& filename, ifstream &fs) {
    fs.open(filename);
    if (!fs.is_open()) {
        cout << "Could not open data set " << filename << endl;
        exit(EXIT_FAILURE);
    }
}

vector<unsigned long> split_string(const string& s) {
    vector<unsigned long> result;
    stringstream ss(s);
    while (ss.good()) {
        string substr;
        getline(ss, substr, ' ');
        if (!substr.empty())
            result.push_back(stoul(substr));
    }
    return result;
}

vector<string> split_string2(const string& s) {
    vector<string> result;
    stringstream ss(s);
    while (ss.good()) {
        string substr;
        getline(ss, substr, ' ');
        if (!substr.empty())
            result.push_back(substr);
    }
    return result;
}



void DataLoader::loadGraph3(const std::string& filepath, bool make_und) {

    struct LEdge {
        unsigned long u,v;
        double c;
    };

    ifstream fs;
    getFileStream(filepath, fs);
    string line;
    vector<LEdge> edges;

    NodeId max_id = 0;
    vector<double> costs;
    while (getline(fs, line)) {
        auto l = split_string2(line);
        auto u = stoul(l[0]);
        auto v = stoul(l[1]);
        auto c = 1.0;
        if (l.size() == 3) c = stod(l[2]);
        edges.push_back(LEdge{u, v, c});
        if (max_id < u) max_id = u;
        if (max_id < v) max_id = v;
    }
    graph->addNodes(max_id);
    graph->loadNodes();
    for (auto &e : edges) {

        NodeId u = e.u;
        NodeId v = e.v;

        NodeP up = graph->getNodeById(u);
        NodeP vp = graph->getNodeById(v);

        up->graph = vp->graph = graph;

        graph->addEdge(up, vp, e.c);
        if (make_und)
            graph->addEdge(vp, up, e.c);

    }
    fs.close();

}


Trajectories loadTrajectories(const string &filepath) {
    ifstream fs;
    getFileStream(filepath, fs);
    string line;

    Trajectories trajectories;

    vector<TraPoint> all_points;
    while (getline(fs, line)) {
        vector<unsigned long> l = split_string(line);
        TraPoint p{};
        p.nid = l[1];
        p.trid = l[0];
        p.interval = TimeInterval{l[2], l[3]};
        all_points.push_back(p);
    }
    fs.close();

    vector<TraPoint> filtered_points;
    for (unsigned int i = 0; i < all_points.size()-1; ++i) {
        auto &p = all_points.at(i);
        auto &n = all_points.at(i+1);
        if (p.interval.start < p.interval.end) {
            assert(p.interval.start < p.interval.end);
            filtered_points.push_back(p);
            continue;
        }
        if (p.trid != n.trid) {
            if (p.interval.start < p.interval.end) {
                filtered_points.push_back(p);
            }
            continue;
        }
        if (!filtered_points.empty()) {
            if (p.trid == n.trid)
                filtered_points.back().interval.end = n.interval.start;
        }
    }

    unsigned long cur_id = 0;
    trajectories.emplace_back();
    trajectories.back().id = cur_id;
    for (auto &p : filtered_points) {
        assert(p.interval.start < p.interval.end);
        if (p.trid != cur_id) {
            trajectories.emplace_back();
            trajectories.back().id = p.trid;
            cur_id++;
        }
        if (!trajectories.empty() && !trajectories.back().points.empty()) {
            assert(trajectories.back().points.back().interval.start < trajectories.back().points.back().interval.end);
        }
        trajectories.back().points.push_back(p);
    }

    sort(trajectories.begin(), trajectories.end(), [](const auto &a, const auto &b){return a.id < b.id;});

    return trajectories;
}


vector<string> split_strings(const string& s) {
    vector<string> result;
    stringstream ss(s);
    while (ss.good()) {
        string substr;
        getline(ss, substr, ' ');
        if (!substr.empty())
            result.push_back(substr);
    }
    return result;
}




void load_distances2(string const &filepath) {
    ifstream fs;
    auto filepath_dists = filepath + "_dists.txt";
    fs.open(filepath_dists);
    if (!fs.is_open()) {
        cout << "Could not open data set " << filepath_dists << endl;
        exit(EXIT_FAILURE);
    }
    string line;
    while (getline(fs, line)) {
        vector<string> l = split_strings(line);
        if (l.size() < 2) continue;
        graph->distances.emplace_back(vector<double>());
        for (auto &s : l) {
            if (s == "-") graph->distances.back().push_back(std::numeric_limits<double>::max());
            else if (!s.empty()) graph->distances.back().push_back(stod(s));
        }
    }
    fs.close();
    cout << "loaded distances from " << filepath_dists << endl;
}


void loadReducedGraph(Trajectories const &trajectories) {
    SGLog::log() << "reducing graph..." << endl;
    auto start = std::chrono::steady_clock::now();

    vector<vector<TrajectoryId>> trajectories_at_nodes(graph->getNumNodes(), vector<TrajectoryId>());
    for (auto &tr : trajectories) {
        for (auto p : tr.points) {
            trajectories_at_nodes.at(p.nid).push_back(tr.id);
        }
    }
    auto const h = (unsigned long) round(sqrt(graph->getNumNodes()));
    vector<pair<unsigned long, unsigned long>> nid_num_itras;
    unsigned long id = 0;
    nid_num_itras.reserve(trajectories_at_nodes.size());
    for (auto &itra : trajectories_at_nodes) {
        nid_num_itras.emplace_back(id++, itra.size());
    }
    sort(nid_num_itras.begin(), nid_num_itras.end(), [](auto const &a, auto const &b){return a.second < b.second;});
    vector<unsigned long> new_vertices;
    for (int i = 0; i < h; ++i) {
        assert(nid_num_itras.at(i).second <= nid_num_itras.at(i+1).second);
        new_vertices.push_back(nid_num_itras.at(i).first);
    }
    reduced_graph->new_vertices = new_vertices;

    vector<vector<unsigned long>> nid_assignment(h);
    vector<unsigned long> nid_assignment_rev(graph->getNumNodes());
    vector<NodeId> nearests;
    for (int i = 0; i < graph->getNumNodes(); ++i) {
        double min_dist = std::numeric_limits<double>::max();
        unsigned long nearest = 0;
        for (int j = 0; j < new_vertices.size(); ++j) {
            double dist = graph->distances.at(i).at(new_vertices.at(j));
            if (dist < min_dist) {
                min_dist = dist;
                nearest = j;
                nearests.clear();
                nearests.push_back(nearest);
            } else if (dist == min_dist) nearests.push_back(j);
        }
        NodeId nearest_nid = nearests.at(rand() % nearests.size());
        nid_assignment.at(nearest_nid).push_back(i);
        nid_assignment_rev.at(i) = nearest_nid;
    }

    reduced_graph->nid_map = nid_assignment_rev;
    reduced_graph->addNodes(h-1);
    reduced_graph->loadNodes();
    reduced_graph->distances.resize(h, vector<double>(h, std::numeric_limits<double>::max()));

    // find new edges
    vector<vector<double>> adjm(h, vector<double>(h, std::numeric_limits<double>::max()));
    for (auto &n : graph->nodes) {
        for (auto e : *n->adjlist) {
            NodeId uid = reduced_graph->nid_map.at(e->u->id);
            NodeId vid = reduced_graph->nid_map.at(e->v->id);
            double c = e->costs;
            if (adjm.at(uid).at(vid) > c) {
                adjm.at(uid).at(vid) = c;
                reduced_graph->distances.at(uid).at(vid) = c;
            }
        }
    }
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < h; ++j) {
            if (adjm.at(i).at(j) != std::numeric_limits<double>::max())
                reduced_graph->addEdge(reduced_graph->getNodeById(i), reduced_graph->getNodeById(j), adjm.at(i).at(j));
        }
    }
    reduced_graph->calculateSpatialDistances();

    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "reduced graph in " << elapsed.count() << " s" << endl;
}



void load_data(string const &graphfile, string const &trajectoryfile, Trajectories &trajectories, Trajectories &queries, unsigned long const num_queries) {
    DataLoader dl;
    dl.loadGraph3(graphfile, false);
    SGLog::log() << "read graph" << endl;
    load_distances2(graphfile);
    Trajectories alltr = loadTrajectories(trajectoryfile);
    vector<bool> is_query(alltr.size(), false);
    auto nq = num_queries;
    while (nq > 0) {
        auto index = rand() % alltr.size();
        if (is_query.at(index)) continue;
        else {
            queries.push_back(alltr.at(index));
            is_query.at(index) = true;
            nq--;
        }
    }
    Id id = 0;
    for (int i = 0; i < alltr.size(); ++i) {
        if (!is_query.at(i)) {
            trajectories.push_back(alltr.at(i));
            trajectories.back().id = id++;
            for (auto &p : trajectories.back().points)
                p.trid = trajectories.back().id;
        }
    }
}


