#ifndef TOPKTRAJ_PARAMS_H
#define TOPKTRAJ_PARAMS_H


#include <string>
#include "Trajectory.h"


void show_help();

struct Params {
    std::string dataset_path;

    unsigned int mode = 0;

    unsigned int num_pivots = 10;

    std::vector<unsigned int> k;

    double radius = 0.5;

    unsigned long rnd_seed = 1;

    unsigned int num_queries = 100;

    unsigned int index = 0;

    bool parseArgs(std::vector<std::string> args);
};

#endif //TOPKTRAJ_PARAMS_H
