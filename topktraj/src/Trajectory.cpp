#include <iostream>
#include "Trajectory.h"
#include "DataLoader.h"

using namespace std;

void Trajectory::initPivot(NodeId nid, TimeInterval ti) {
    points.emplace_back(TraPoint{nid, ti});
}

std::ostream& operator<<(std::ostream& os, const Trajectory &t) {
    std::string s1;

    int maxout = 10;
    for (auto &p : t.points) {
        s1 += "(" + to_string(p.nid) + ", [" + to_string(p.interval.start) + "," + to_string(p.interval.end) + "]), ";
        maxout--;
        if (maxout == 0) break;
    }
    s1 = s1.substr(0, s1.size()-2);
    if (t.points.size() > 10) s1 += "...";

    return os << s1 << endl;
}

unsigned long sizeOfTimeInterval(const TimeInterval &t) {
    auto result = t.end - t.start;
    return result;
}

unsigned long sizeOfCutTimeIntervals(const TimeInterval &a, const TimeInterval &b) {
    if (b.end <= a.start || a.end <= b.start) return 0;
    TimeInterval t{max(a.start, b.start), min(a.end, b.end)};
    return sizeOfTimeInterval(t);
}

bool timeIntervalsIntersect(const TimeInterval &a, const TimeInterval &b) {
    if (b.end < a.start || a.end < b.start) return false;
    return true;
}

unsigned long sizeOfCutTimeIntervals(const TimeInterval &a, const TimeInterval &b, const TimeInterval &t) {
    if (t.start > a.end || t.end < a.start || t.start > b.end || t.end < b.start) return 0;
    if (b.end < a.start || a.end < b.start) return 0;
    TimeInterval s = a;
    if (a.start < t.start) s.start = t.start;
    if (a.end > t.end) s.end = t.end;
    TimeInterval r = b;
    if (b.start < t.start) r.start = t.start;
    if (b.end > t.end) r.end = t.end;
    TimeInterval q{max(s.start, r.start), min(s.end, r.end)};
    return sizeOfTimeInterval(q);
}

std::string to_string(const TimeInterval &t) {
    string s = "[";
    s += to_string(t.start) + "," + to_string(t.end) + "]";
    return s;
}

Trajectory shrink(const Trajectory &trajectory) {
    Trajectory result;

    for (int i = 0; i < trajectory.points.size(); ++i) {
        TraPoint p = trajectory.points.at(i);
        while (i < trajectory.points.size()-1 && reduced_graph->nid_map.at(trajectory.points.at(i+1).nid) == reduced_graph->nid_map.at(p.nid)) {
            ++i;
        }
        p.interval.end = trajectory.points.at(i).interval.end;
        p.nid = reduced_graph->getNewVertex(p.nid);
        result.points.push_back(p);
    }
    result.id = trajectory.id;
    return result;
}

Trajectory shrink_RG(const Trajectory &trajectory) {
    Trajectory result;

    for (int i = 0; i < trajectory.points.size(); ++i) {
        TraPoint p = trajectory.points.at(i);
        while (i < trajectory.points.size()-1 && reduced_graph->nid_map.at(trajectory.points.at(i+1).nid) == reduced_graph->nid_map.at(p.nid)) {
            ++i;
        }
        p.interval.end = trajectory.points.at(i).interval.end;
        p.nid = reduced_graph->nid_map.at(p.nid);
        result.points.push_back(p);
    }
    result.id = trajectory.id;
    return result;
}

