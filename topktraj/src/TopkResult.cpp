#include "TopkResult.h"
#include "Trajectory.h"

using namespace std;

void TopkResult::insert(Id id, const double value) {
    if (doApproximation() && minMaxTopK > value) return;

    auto p = topk.insert(TopKEntry(value, id));
    if (!p.second) {
        const_cast<std::vector<NodeId>&>(p.first->ids).push_back(id);
    }

    if (minMaxTopK < value && topk.size() > k) {
        auto i = topk.find(TopKEntry(minMaxTopK, 0));
        if (i != topk.end()) {
            topk.erase(i);
        }
    }

    minMaxTopK = topk.rbegin()->value;
}

Stats TopkResult::countTopkSet(TopkResult &result) {
    double sum_sim_lhs = 0.0;
    double sum_sim_rhs = 0.0;
    for (auto &e : topk) {
        sum_sim_lhs += e.value;
    }
    for (auto &f : result.topk) {
        sum_sim_rhs += f.value;
    }

    Stats stats{};

    if (sum_sim_lhs != 0) stats.ssr_inv = sum_sim_rhs / sum_sim_lhs;
    else stats.ssr_inv = 1.0;

    return stats;
}

