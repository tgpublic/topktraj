#include "AbstractIndex.h"
#include "SpatialDS.h"
#include "TimeIntervalTree.h"
#include "BaselineIndex.h"
#include "BaselineIndexApprox.h"

using namespace std;

shared_ptr<AbstractIndex> getIndex(const Params& params, const Trajectories &trajectories, const TimeInterval &timeInterval) {
    shared_ptr<AbstractIndex> index;
    switch (params.index) {
        case 0 : {
            auto i = init_tit(trajectories, params.num_pivots);
            index = make_shared<TimeIntervalTree>(std::move(i));
            break;
        }
        case 1 : {
            auto i = init_sds(trajectories, timeInterval, params.num_pivots);
            index = make_shared<SpatialDS>(i);
            break;
        }
        case 2 : {
            auto i = init_sea_baseline(trajectories);
            index = make_shared<BaselineIndex>(std::move(i));
            break;
        }
        case 3 : {
            auto i = init_baseline_approx_SHQ(trajectories);
            index = make_shared<BaselineIndexApprox>(std::move(i));
            break;
        }
        case 4 : {
            auto i = init_baseline_approx_SHQT(trajectories);
            index = make_shared<BaselineIndexApprox>(std::move(i));
            break;
        }
    }
    return index;
}