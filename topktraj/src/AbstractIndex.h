#ifndef TOPKTRAJ_ABSTRACTINDEX_H
#define TOPKTRAJ_ABSTRACTINDEX_H

#include "Trajectory.h"
#include "Params.h"

struct AbstractIndex {

    virtual Trajectories query(Trajectory &q, TimeInterval &t, double r) = 0;

    virtual void reset() {};

    virtual std::string getName() = 0;

};

std::shared_ptr<AbstractIndex> getIndex(const Params& params, const Trajectories &trajectories, const TimeInterval &timeInterval);


#endif //TOPKTRAJ_ABSTRACTINDEX_H
