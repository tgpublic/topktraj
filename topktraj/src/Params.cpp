#include <vector>
#include "Params.h"
#include "SGLog.h"

using namespace std;

void show_help() {
    std::cout << "topktraj dataset action [-i=index] [-k=value for top-k] [-r=radius] [-p=num pivots] [-q=num queries]" << std::endl;
    std::cout << "\nactions:" << std::endl;
    std::cout << "0\t top-k query" << std::endl;
    std::cout << "1\t show trajectory stats" << std::endl;
    std::cout << "\nindex:" << std::endl;
    std::cout << "0\t tree" << std::endl;
    std::cout << "1\t pivot" << std::endl;
    std::cout << "2\t sea baseline" << std::endl;
    std::cout << "3\t sea shq" << std::endl;
    std::cout << "4\t seq shqt" << std::endl;
}

bool Params::parseArgs(vector<string> args) {
    if (args.size() < 2) {
        return false;
    }
    for (auto & arg : args) {
        SGLog::log() << arg << " ";
    }
    SGLog::log() << endl;

    try {
        dataset_path = args[0];
        mode = stoi(args[1]);
    } catch (...) {
        return false;
    }

    for (size_t i = 2; i < args.size(); ++i) {
        string next = args[i];
        if (next.length() < 4) return false;
        if (next.substr(0, 3) == "-p=") {
            string valstr = next.substr(3);
            num_pivots = stoi(valstr);
            SGLog::log() << "set num pivots " << num_pivots << endl;
        } else
        if (next.substr(0, 3) == "-r=") {
            string valstr = next.substr(3);
            radius = stod(valstr);
            SGLog::log() << "set radius " << radius << endl;
        } else
        if (next.substr(0, 3) == "-y=") {
            string valstr = next.substr(3);
            rnd_seed = stoul(valstr);
            SGLog::log() << "set rnd seed " << rnd_seed << endl;
        } else
            if (next.substr(0, 3) == "-i=") {
            string valstr = next.substr(3);
            index = stoul(valstr);
            SGLog::log() << "set index " << index << endl;
        } else
        if (next.substr(0, 3) == "-q=") {
            string valstr = next.substr(3);
            num_queries = stoi(valstr);
            SGLog::log() << "set num queries " << num_queries << endl;
        } else
        if (next.substr(0, 3) == "-k=") {
            string valstr = next.substr(3);
            k.push_back(stoi(valstr));
            SGLog::log() << "added k " << k.back() << endl;
        } else {
            return false;
        }
    }
    return true;
}