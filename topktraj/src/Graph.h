#ifndef TOPKTRAJ_GRAPH_H
#define TOPKTRAJ_GRAPH_H

#include <memory>
#include <utility>
#include <vector>
#include <unordered_map>
#include <list>
#include <set>
#include <cassert>
#include <iostream>


using Id = unsigned long;
using NodeId = Id;

struct Edge;
struct Node;
struct Graph;
using EdgeP = std::shared_ptr<Edge>;
using NodeP = std::shared_ptr<Node>;
using GraphP = std::shared_ptr<Graph>;

struct Edge {
    EdgeP next;
    EdgeP prev;
    NodeP u;
    NodeP v;
    double costs {};
};

struct Adjlist;

struct Node {
    NodeId id {};
    std::shared_ptr<Adjlist> adjlist;
    std::set<EdgeP> incomingEdges;

    NodeP next;
    NodeP prev;

    GraphP graph;

    [[nodiscard]] NodeP getRandomNeighbour() const;

};


struct Adjlist {
    EdgeP firstOut;
    EdgeP lastOut;

    class iterator
    {
    public:
        typedef std::forward_iterator_tag iterator_category;
        explicit iterator(EdgeP ptr) : edge(std::move(ptr)) { }
        iterator operator++() { iterator i = *this; edge = edge->next; return i; }
        EdgeP operator*() { return edge; }
        EdgeP operator->() { return edge; }
        bool operator!=(const iterator& rhs) { return edge != rhs.edge; }
    private:
        EdgeP edge;
    };

    iterator begin() const { return iterator(firstOut); }

    iterator end() const { return iterator(nullptr); }

    void addEdge(const EdgeP& e);

    void addEdge(NodeP u, NodeP v, double costs);

    [[nodiscard]] unsigned long getNumEdges() const {return numEdges;}

private:

    unsigned long numEdges{};

};

class Graph {

public:

    Graph() = default;

    Graph(const Graph &rhs) {
        firstNode = rhs.firstNode;
        lastNode = rhs.lastNode;
        nodes = rhs.nodes;
        nodemap = rhs.nodemap;
        num_nodes = rhs.num_nodes;
        distances = rhs.distances;
    }

    Graph& operator=(const Graph &rhs) {
        if (this == &rhs) return *this;
        firstNode = rhs.firstNode;
        lastNode = rhs.lastNode;
        nodes = rhs.nodes;
        nodemap = rhs.nodemap;
        num_nodes = rhs.num_nodes;
        distances = rhs.distances;
        return *this;
    }

  class nodeIterator
    {
    public:
        typedef std::forward_iterator_tag iterator_category;
        explicit nodeIterator(NodeP ptr) : node(std::move(ptr)) { }
        nodeIterator operator++() { nodeIterator i = *this; node = node->next; return i; }
        Node operator*() { return *node.get(); }
        NodeP operator->() { return node; }
        bool operator!=(const nodeIterator& rhs) { return node != rhs.node; }
    private:
        NodeP node;
    };

    nodeIterator begin() const {
        return nodeIterator(firstNode);
    }

    nodeIterator end() const {
        return nodeIterator(nullptr);
    }

    NodeP addNode(NodeId nid) {
        auto it = nodemap.find(nid);
        if (it != nodemap.end()) return it->second;

        NodeP node = std::make_shared<Node>();
        node->id = nid;
        node->adjlist = std::make_shared<Adjlist>();
        nodemap.insert({node->id, node});
        if (firstNode == nullptr && lastNode == nullptr) {
            firstNode = lastNode = node;
        } else {
            lastNode->next = node;
            node->prev = lastNode;
            node->next = nullptr;
            lastNode = node;
        }
        ++num_nodes;
        return node;
    }

    void addNodes(NodeId max_id) {
        for (int nid = 0; nid <= max_id; ++nid) {
            addNode(nid);
        }
    }

    void loadNodes() {
        nodes.resize(num_nodes);
        for (auto &n : nodemap)
            nodes.at(n.second->id) = n.second;

        for (int i = 0; i < nodes.size(); ++i) {
            assert(i == nodes.at(i)->id);
        }
    }

    void addEdge(const NodeP& u, const NodeP&  v, double costs) {
        u->adjlist->addEdge(u, v, costs);
        ++num_edges;
    }

    unsigned long getNumNodes() const {return num_nodes;}
    unsigned long getNumEdges() const {return num_edges;}

    double getDistance(NodeId u, NodeId v) const {
        return distances.at(u).at(v);
    }

    NodeP getNodeById(NodeId nid) {
        return nodes.at(nid);
    }

    void calculateSpatialDistances();

    auto getRandomNodeP() const {
        auto n = std::next(std::begin(nodemap), rand() % nodemap.size());
        assert(n != nodemap.end());
        return n;
    }

    std::vector<std::vector<double>> distances;

    // for reduced graph
    std::vector<NodeId> nid_map;
    std::vector<NodeId> new_vertices;
    NodeId getNewVertex(const NodeId nid) {
        return new_vertices.at(nid_map.at(nid));
    }

    std::vector<NodeP> nodes;
    std::unordered_map<NodeId, NodeP> nodemap;

private:
    NodeP firstNode;
    NodeP lastNode;

    unsigned long num_nodes{};
    unsigned long num_edges{};

};

using GraphP = std::shared_ptr<Graph>;

extern GraphP reduced_graph;



#endif //TOPKTRAJ_GRAPH_H
