#include <chrono>
#include "SpatialDS.h"
#include "SGLog.h"

using namespace std;

void SpatialDS::init(TimeInterval t, Trajectories _trajectories) {
    timeInterval = t;
    trajectories = std::move(_trajectories);

    if (graph->getNumNodes() < num_pivots) num_pivots = graph->getNumNodes();

    vector<unsigned int> num_traj_at_node(graph->getNumNodes(), 0);
#pragma omp parallel for default(none) shared(num_traj_at_node, trajectories)
    for (int i = 0; i < trajectories.size(); ++i) {
        for (auto &p : trajectories.at(i).points) {
            num_traj_at_node.at(p.nid)++;
        }
    }

    vector<pair<NodeId, unsigned int>> ntn;
    ntn.reserve(num_traj_at_node.size());

    ntn.resize(graph->getNumNodes());
#pragma omp parallel for default(none) shared(graph, ntn, num_traj_at_node)
    for (NodeId nid = 0; nid < graph->getNumNodes(); ++nid) {
        ntn.at(nid) = {nid, num_traj_at_node.at(nid)};
    }
    sort(ntn.begin(), ntn.end(), [](auto &a, auto &b){return a.second > b.second;});

    traj_pivot_dists.resize(num_pivots);
    pivots.resize(num_pivots);
#pragma omp parallel for default(none) shared(ntn, t, num_pivots, traj_pivot_dists, trajectories, pivots, graph)
    for (int i = 0; i < num_pivots; ++i) {
        Trajectory pivot;
        pivot.initPivot(ntn.at(i).first, t);

        traj_pivot_dists.at(i).clear();
        for (unsigned long j = 0; j < trajectories.size(); ++j) {
            double d = 1.0 - sim(pivot, trajectories.at(j), pivot.getTimeInterval(), graph->distances);
            traj_pivot_dists.at(i).emplace_back(j, d);
        }
        pivots.at(i) = pivot;
    }
}

Trajectories SpatialDS::query(Trajectory &q, TimeInterval &t, double r) {
    if (trajectories.empty()) return Trajectories();
    Trajectories result;

#pragma omp parallel for default(none) shared(r, graph, num_pivots, traj_pivot_dists, trajectories, q)
    for (int i = 0; i < num_pivots; ++i) {
        auto &pivot = pivots.at(i);
        double d = 1.0 - sim(pivot, q, pivot.getTimeInterval(), graph->distances);
        for (auto &p : traj_pivot_dists.at(i)) {

            double lb = fabs(p.second - d);
            if (lb > r) {
                trajectories.at(p.first).usetr = false;
            }
        }
    }

    for (auto & tr : trajectories) {
        if (tr.usetr) result.push_back(tr);
    }
    return result;
}

void SpatialDS::reset() {
    for (auto &t : trajectories)
        t.usetr = true;
}


SpatialDS init_sds(Trajectories const &trajectories, const TimeInterval &gt, unsigned long num_pivots) {
    SGLog::log() << "init sds" << endl;
    auto start = std::chrono::steady_clock::now();
    SpatialDS sds(num_pivots);
    sds.init(gt, trajectories);
    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "initialized sds index in " << elapsed.count() << " s" << endl;
    return sds;
}