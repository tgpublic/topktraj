//
// Created by lutz on 14.07.20.
//

#ifndef TOPKTRAJ_DATALOADER_H
#define TOPKTRAJ_DATALOADER_H

#include "Graph.h"
#include "Trajectory.h"

extern GraphP graph;
extern GraphP reduced_graph;


Trajectories loadTrajectories(const std::string &filepath);

void load_distances2(std::string const &filepath);

void loadReducedGraph(Trajectories const &trajectories);

void load_data(std::string const &graphfile, std::string const &trajectoryfile, Trajectories &trajectories, Trajectories &queries, unsigned long num_queries);

class DataLoader {

public:

    void loadGraph3(const std::string& filepath, bool make_und);

};


#endif //TOPKTRAJ_DATALOADER_H
