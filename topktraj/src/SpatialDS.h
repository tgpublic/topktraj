//
// Created by lutz on 24.08.20.
//

#ifndef TOPKTRAJ_SPATIALDS_H
#define TOPKTRAJ_SPATIALDS_H

#include <cmath>
#include <cassert>
#include <iostream>
#include "Trajectory.h"
#include "TrajectoryIndex.h"
#include "AbstractIndex.h"


struct SpatialDS : public AbstractIndex {

    SpatialDS() = default;

    explicit SpatialDS(unsigned int num_pivots) : num_pivots(num_pivots) {};

    void init(TimeInterval t, Trajectories _trajectories);

    Trajectories query(Trajectory &q, TimeInterval &t, double r) override;

    Trajectories trajectories;

    void reset() override;

    std::string getName() override {
        return "PIVOT index";
    }

private:
    unsigned int num_pivots{};
    Trajectories pivots;
    std::vector<std::vector<std::pair<TrajectoryId, double>>> traj_pivot_dists;
    TimeInterval timeInterval{};
};

SpatialDS init_sds(Trajectories const &trajectories, TimeInterval const &gt, unsigned long num_pivots);

#endif //TOPKTRAJ_SPATIALDS_H
