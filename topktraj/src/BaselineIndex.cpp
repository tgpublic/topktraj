#include <chrono>
#include "BaselineIndex.h"
#include "TimeIntervalTree.h"
#include "SGLog.h"
#include <algorithm>

using namespace std;

ITnode* IT::findStart(TimeInterval &t, ITnode *r, TrajectoryIds &trids) {
    TrajectoryIds qtr = r->getTrajectoryIds(t);
    trids.insert(trids.end(), qtr.begin(), qtr.end());
    if (r->left_child != nullptr) {
        if (t.end <= r->left_child->timeInterval.end) {
            r = findStart(t, r->left_child.get(), trids);
        }
    }
    if (r->right_child != nullptr) {
        if (t.start >= r->right_child->timeInterval.start) {
            r = findStart(t, r->right_child.get(), trids);
        }
    }
    return r;
}

void IT::collect(TimeInterval &t, ITnode *r, TrajectoryIds &trids) {
    if (r->left_child != nullptr) {
        collect(t, r->left_child.get(), trids);
    }
    if (r->right_child != nullptr) {
        collect(t, r->right_child.get(), trids);
    }

    TrajectoryIds qtr = r->getTrajectoryIds(t);
    trids.insert(trids.end(), qtr.begin(), qtr.end());
}

TrajectoryIds IT::query(TimeInterval t) {
    TrajectoryIds result;
    auto n = findStart(t, root.get(), result);
    if (n->left_child != nullptr) collect(t, n->left_child.get(), result);
    if (n->right_child != nullptr) collect(t, n->right_child.get(), result);
    return result;
}

void IT::construct(TPairs tPairs) {
    root = std::make_unique<ITnode>();
    root->parent = nullptr;
    construct_children(root.get(), tPairs);
}

SME findSME2(TPairs &tPairs) {
    unsigned long  start = UINT64_MAX;
    unsigned long end = 0;
    vector<Time> ends;
    for (auto &t : tPairs) {
        if (t.interval.start < start) start = t.interval.start;
        if (t.interval.end > end) end = t.interval.end;
        ends.push_back(t.interval.end);
        ends.push_back(t.interval.start);
    }
    sort(ends.begin(), ends.end());
    assert(!ends.empty());
    Time mid = ends.at(ends.size() / 2);
    return SME{start, mid, end};
}

void IT::construct_children(ITnode* r, TPairs &tPairs) {
    if (tPairs.empty()) return;

    SME sme = findSME2(tPairs);
    auto start = sme.start;
    auto mid = sme.mid;
    auto end = sme.end;

    if (tPairs.size() <= leaf_bucket_size || end - start <= leaf_bucket_min_time || mid == end || start == mid) { //todo
        r->left_child = nullptr;
        r->right_child = nullptr;
        r->tpairs = tPairs;
        r->timeInterval = TimeInterval{start, end};
        return;
    }

    TPairs left, right, rpairs;
    for (auto & t : tPairs) {
        if (t.interval.start >= start && t.interval.end <= mid) {
            left.push_back(t);
        } else if (t.interval.start >= mid && t.interval.end <= end) {
            right.push_back(t);
        } else {
            rpairs.push_back(t);
        }
    }
    r->tpairs = rpairs;

    if (!left.empty()) {
        r->left_child = std::make_unique<ITnode>();
        r->left_child->parent = r;
        construct_children(r->left_child.get(), left);
    }

    if (!right.empty()) {
        r->right_child = std::make_unique<ITnode>();
        r->right_child->parent = r;
        construct_children(r->right_child.get(), right);
    }

    r->timeInterval = TimeInterval{start, end};
}

void BaselineIndex::init(Trajectories trajectories1) {
    trajectories = std::move(trajectories1);

    auto cmp = [](TPair a, TPair b) {
        return a.trid < b.trid || a.interval.start < b.interval.start || a.interval.end < b.interval.end;
    };
    vector<set<TPair, decltype(cmp)>> tpairs(graph->getNumNodes(), set<TPair, decltype(cmp)>(cmp));

    for (auto &t : trajectories) {
        for (auto &p : t.points) {

            TPair tpair{};
            tpair.interval = p.interval;
            tpair.trid = t.id;
            tpair.nid = p.nid;
            assert(t.id == p.trid);
            tpairs.at(p.nid).insert(tpair);
            auto node = graph->getNodeById(p.nid);
            for (auto e : *node->adjlist) {
                TPair tp{};
                tp.interval = p.interval;
                tp.trid = t.id;
                tp.nid = e->v->id;
                tpairs.at(tp.nid).insert(tp);
            }
        }
    }

    for (auto &tpair : tpairs) {
        intervaltrees.emplace_back();
        intervaltrees.back().construct(TPairs(tpair.begin(), tpair.end()));
    }

}

Trajectories BaselineIndex::query(Trajectory &Q, TimeInterval &t, double) {
    set<TrajectoryId> trid_set;
    for (auto &p : Q.points) {
        auto trids = intervaltrees.at(p.nid).query(p.interval);
        trid_set.insert(trids.begin(), trids.end());
    }

    Trajectories result;
    for (auto &id : trid_set)
        result.push_back(trajectories.at(id));

    return result;
}

TrajectoryIds ITnode::getTrajectoryIds(TimeInterval &interval) {
    TrajectoryIds result;
    for (auto p : tpairs) {
        if (timeIntervalsIntersect(interval, p.interval))
            result.push_back(p.trid);
    }
    return result;
}


BaselineIndex init_sea_baseline(Trajectories const &trajectories) {
    SGLog::log() << "init sea baseline" << endl;
    auto start = std::chrono::steady_clock::now();
    BaselineIndex baselineIndex;
    baselineIndex.init(trajectories);
    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "initialized sea index in " << elapsed.count() << " s" << endl;
    return baselineIndex;
}