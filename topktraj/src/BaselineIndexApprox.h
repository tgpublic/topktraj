#ifndef TOPKTRAJ_BASELINEINDEXAPPROX_H
#define TOPKTRAJ_BASELINEINDEXAPPROX_H


#include "Trajectory.h"
#include "BaselineIndex.h"
#include <algorithm>


struct BaselineIndexApprox : public AbstractIndex {

    void init(Trajectories trajectories1);

    Trajectories query(Trajectory &Q, TimeInterval &t, double) override;

    Trajectories trajectories;

    std::vector<IT> intervaltrees;

    std::string getName() override {
        return "SEA SHQ(T) index";
    }

};

BaselineIndexApprox init_baseline_approx_SHQ(Trajectories const &trajectories);
BaselineIndexApprox init_baseline_approx_SHQT(Trajectories const &trajectories);

#endif //TOPKTRAJ_BASELINEINDEXAPPROX_H
