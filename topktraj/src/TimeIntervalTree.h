#ifndef TOPKTRAJ_TIMEINTERVALTREE_H
#define TOPKTRAJ_TIMEINTERVALTREE_H

#include "Trajectory.h"
#include "SpatialDS.h"

struct SME {
    Time start;
    Time mid;
    Time end;
};


struct TINode {
    explicit TINode(unsigned int num_pivots) : sds(num_pivots){};
    TimeInterval timeInterval{};
    std::unique_ptr<TINode> left_child;
    std::unique_ptr<TINode> right_child;
    TINode* parent{};
    SpatialDS sds;

    void init(Trajectories &trajectories1, TimeInterval timeInterval1) {
        timeInterval = timeInterval1;
        sds.init(timeInterval1, trajectories1);
    }

};

struct TimeIntervalTree : public AbstractIndex {

    TimeIntervalTree() = default;

    explicit TimeIntervalTree(unsigned long num_pivots) : num_pivots(num_pivots) {}

    std::unique_ptr<TINode> root;

    void buildTree(Trajectories trajectories);

    Trajectories query(Trajectory &q, TimeInterval &t, double radius) override;

    std::string getName() override {
        return "TREE index";
    }

    void reset() override;

    void resetNode(TINode* n);

private:

    std::unique_ptr<TINode> construct_children(std::unique_ptr<TINode> r, Trajectories &trajectories);

    TINode* findStart(TimeInterval &t, TINode *r, Trajectory &q, Trajectories &result, double radius);

    void collect(Trajectory &q, TimeInterval &t, TINode *r, Trajectories &trajectories, double radius);

    unsigned int leaf_bucket_size = 10;
    Time leaf_bucket_min_time = 10;

    unsigned int num_pivots = 10;

};

TimeIntervalTree init_tit(Trajectories trajectories, unsigned long num_pivots);

#endif //TOPKTRAJ_TIMEINTERVALTREE_H
