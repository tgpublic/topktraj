#include <iostream>
#include <chrono>
#include "TimeIntervalTree.h"
#include "SGLog.h"

using namespace std;

SME findSME(Trajectories &trajectories) {
    unsigned long  start = UINT64_MAX;
    unsigned long end = 0;
    vector<Time> ends;
    for (auto &t : trajectories) {
        if (t.getStartTime() < start) start = t.getStartTime();
        if (t.getEndTime() > end) end = t.getEndTime();
        ends.push_back(t.getEndTime());
        ends.push_back(t.getStartTime());
    }
    sort(ends.begin(), ends.end());
    Time mid = ends.at(ends.size() / 2);
    return SME{start, mid, end};
}

unique_ptr<TINode> TimeIntervalTree::construct_children(unique_ptr<TINode> r, Trajectories &trajectories) {
    if (trajectories.empty()) return r;

    SME sme = findSME(trajectories);
    auto start = sme.start;
    auto mid = sme.mid;
    auto end = sme.end;

    if (trajectories.size() <= leaf_bucket_size || end - start <= leaf_bucket_min_time) {
        r->left_child = nullptr;
        r->right_child = nullptr;
        r->init(trajectories, TimeInterval{start, end});
        return r;
    }

    Trajectories left, right;

    Trajectories rtrajectories;
    for (auto & t : trajectories) {
        if (t.getStartTime() >= start && t.getEndTime() <= mid) {
            left.push_back(t);
        } else if (t.getStartTime() >= mid && t.getEndTime() <= end) {
            right.push_back(t);
        } else {
            rtrajectories.push_back(t);
        }
    }
    r->init(rtrajectories, TimeInterval{start, end});

    if (!left.empty()) {
        r->left_child = std::make_unique<TINode>(num_pivots);
        r->left_child->parent = r.get();
        r->left_child = construct_children(std::move(r->left_child), left);
    }

    if (!right.empty()) {
        r->right_child = std::make_unique<TINode>(num_pivots);
        r->right_child->parent = r.get();
        r->right_child = construct_children(std::move(r->right_child), right);
    }

    return r;
}

void TimeIntervalTree::buildTree(Trajectories trajectories) {
    sort(trajectories.begin(), trajectories.end(), [](const Trajectory &a, const Trajectory &b){
        return a.getEndTime() < b.getEndTime();
    });
    stable_sort(trajectories.begin(), trajectories.end(), [](const Trajectory &a, const Trajectory &b){
        return a.getStartTime() < b.getStartTime();
    });
    root = std::make_unique<TINode>(num_pivots);
    root->parent = nullptr;
    root = construct_children(std::move(root), trajectories);
}

Trajectories TimeIntervalTree::query(Trajectory &q, TimeInterval &t, double radius) {
    Trajectories result;
    auto n = findStart(t, root.get(), q, result, radius);
    if (n->left_child != nullptr) collect(q, t, n->left_child.get(), result, radius);
    if (n->right_child != nullptr) collect(q, t, n->right_child.get(), result, radius);
    return result;
}


TINode* TimeIntervalTree::findStart(TimeInterval &t, TINode *r, Trajectory &q, Trajectories &result, double radius) {
    Trajectories qtr = r->sds.query(q, t, radius);
    result.insert(result.end(), qtr.begin(), qtr.end());
    if (r->left_child != nullptr) {
        if (t.end <= r->left_child->timeInterval.end) {
            r = findStart(t, r->left_child.get(), q, result, radius);
        }
    }
    if (r->right_child != nullptr) {
        if (t.start >= r->right_child->timeInterval.start) {
            r = findStart(t, r->right_child.get(), q, result, radius);
        }
    }
    return r;
}

void TimeIntervalTree::collect(Trajectory &q, TimeInterval &t, TINode *r, Trajectories &trajectories, double radius) {
    if (r->left_child != nullptr) {
        collect(q, t, r->left_child.get(), trajectories, radius);
    }
    if (r->right_child != nullptr) {
        collect(q, t, r->right_child.get(), trajectories, radius);
    }

    Trajectories qtr = r->sds.query(q, t, radius);
    trajectories.insert(trajectories.end(), qtr.begin(), qtr.end());
}

void TimeIntervalTree::resetNode(TINode* n) {
    if (n == nullptr) return;
    n->sds.reset();
    resetNode(n->left_child.get());
    resetNode(n->right_child.get());
}

void TimeIntervalTree::reset() {
    resetNode(root.get());
}

TimeIntervalTree init_tit(Trajectories trajectories, unsigned long num_pivots) {
    SGLog::log() << "init interval tree" << endl;
    auto start = std::chrono::steady_clock::now();
    TimeIntervalTree tit(num_pivots);
    tit.buildTree(std::move(trajectories));
    auto finish = std::chrono::steady_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    SGLog::log() << "initialized tree index in " << elapsed.count() << " s" << endl;
    return tit;
}