#ifndef CENTRALITY_TOPKRESULT_H
#define CENTRALITY_TOPKRESULT_H

#include "Graph.h"
#include "Trajectory.h"
#include <set>
#include <memory>

struct Stats {
    double ssr_inv = 0.0;
};

struct comp
{
    template<typename T>
    bool operator()(const T& l, const T& r) const {
        return l.value > r.value;
    }
};

struct TopKEntry {

    TopKEntry(double value, Id id) : value(value) {
        ids.push_back(id);
    };

    ~TopKEntry() = default;

    double value = 0;
    std::vector<Id> ids;
};

class TopkResult {

public:

    explicit TopkResult(const unsigned int k) :
            k(k), minMaxTopK(-1){
    }

    void insert(Id id, double value);

    void insert_range(Id id, const double value);

    [[nodiscard]] double getMinMaxTopk() const {
        return minMaxTopK;
    }

    bool doApproximation() {
        return topk.size() >= k;
    }

    Stats countTopkSet(TopkResult &result);

    unsigned long size() {
        unsigned long s = 0;
        for (auto &tk : topk) {
            s += tk.ids.size();
        }
        return s;
    }

private:

    unsigned int k;

    double minMaxTopK;

    std::set<TopKEntry, comp> topk;

};

using TopkResults = std::vector<TopkResult>;

void print_topkresults(TopkResult &topkResults, Trajectories &trajectories, Trajectory &Q);

#endif //CENTRALITY_TOPKRESULT_H
