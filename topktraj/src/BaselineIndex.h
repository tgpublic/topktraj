#ifndef TOPKTRAJ_BASELINEINDEX_H
#define TOPKTRAJ_BASELINEINDEX_H


#include <utility>

#include "Trajectory.h"
#include "AbstractIndex.h"

struct TPair {
    TimeInterval interval;
    TrajectoryId trid;
    NodeId nid;
};

using TPairs = std::vector<TPair>;

struct ITnode {
    TimeInterval timeInterval{};
    std::unique_ptr<ITnode> left_child;
    std::unique_ptr<ITnode> right_child;
    ITnode* parent{};
    TPairs tpairs;

    TrajectoryIds getTrajectoryIds(TimeInterval &interval);
};

struct IT {
    void construct(TPairs tPairs);
    TrajectoryIds query(TimeInterval t);
    std::unique_ptr<ITnode> root;

    void construct_children(ITnode* node, TPairs &tPairs);

    unsigned long leaf_bucket_size = 10;
    unsigned long leaf_bucket_min_time = 10;

private:
    ITnode* findStart(TimeInterval &t, ITnode *r, TrajectoryIds &trids);
    void collect(TimeInterval &t, ITnode *r, TrajectoryIds &trids);
};

struct BaselineIndex : public AbstractIndex {

    void init(Trajectories trajectories1);

    std::string getName() override {
        return "SEA Baseline index";
    }

    Trajectories query(Trajectory &Q, TimeInterval &t, double) override;

    Trajectories trajectories;

    std::vector<IT> intervaltrees;

};

BaselineIndex init_sea_baseline(Trajectories const &trajectories);

#endif //TOPKTRAJ_BASELINEINDEX_H
