# Spatio-Temporal Top-k Similarity Search for Trajectories in Graphs

Source code for our paper *"[Spatio-Temporal Top-k Similarity Search for Trajectories in Graphs](https://arxiv.org/abs/2009.06778)"*.


## Data sets

The data sets are in the folder datasets. 
Unzip the file `topktraj_datasets.zip`. 

## Build

In folder `topktraj` run:
```
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## Running the experiments

After building use `./topktraj` copy `run_all_experiments.sh` to release folder and run:
```
chmod +x run_all_experiments.sh
./run_all_experiments.sh [path to datasets]
```
where `[path to datasets]` needs to be replaced with the path to the folder that contains the data set files.

The results are saved to `logfile.txt`.

## Terms and conditions
Please feel free to use our code. We only ask that you cite:

    @article{driemel2020spatio,
        title={Spatio-Temporal Top-k Similarity Search for Trajectories in Graphs},
        author={Driemel, Anne and Mutzel, Petra and Oettershagen, Lutz},
        journal={arXiv preprint arXiv:2009.06778},
        year={2020}
    }


## Contact 
If you have any questions, send an email to Lutz Oettershagen (lutz.oettershagen at cs.uni-bonn.de).

