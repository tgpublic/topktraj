#!/bin/bash

./topktraj $1/facebook1 0 -i=0 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=8 -r=.1 
./topktraj $1/facebook1 0 -i=1 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=8 -r=.1
./topktraj $1/facebook1 0 -i=2 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/facebook1 0 -i=3 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/facebook1 0 -i=4 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100

./topktraj $1/facebook2 0 -i=0 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=8 -r=.1 
./topktraj $1/facebook2 0 -i=1 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=8 -r=.1 
./topktraj $1/facebook2 0 -i=2 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/facebook2 0 -i=3 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/facebook2 0 -i=4 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100

./topktraj $1/milan 0 -i=0 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=32 -r=0.02
./topktraj $1/milan 0 -i=1 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=16 -r=0.02  
./topktraj $1/milan 0 -i=2 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/milan 0 -i=3 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/milan 0 -i=4 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100

./topktraj $1/tdrive 0 -i=0 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=64 -r=0.2 
./topktraj $1/tdrive 0 -i=1 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100 -p=64 -r=0.25 
./topktraj $1/tdrive 0 -i=2 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/tdrive 0 -i=3 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100
./topktraj $1/tdrive 0 -i=4 -k=1 -k=1 -k=2 -k=4 -k=8 -k=16 -k=32 -k=64 -q=100

